import shutil
import os
from send2trash import send2trash as send2trash_original


def path_generator(path, patterns):
    paths = []
    for root, _, file in os.walk(path):
        for f in file:
            for pattern in patterns:
                if f.endswith(pattern):
                    paths.append(os.path.join(root, f))
    return sorted(paths)


def send2trash(path):
    video_path = os.path.join(os.path.expanduser('~/VIDEO_pybpod'))
    files = [os.path.basename(os.path.normpath(path)) + '.avi',
             os.path.basename(os.path.normpath(path)) + '.npy',
             os.path.basename(os.path.normpath(path)) + '.npz',
             os.path.basename(os.path.normpath(path)) + '_1.avi',
             os.path.basename(os.path.normpath(path)) + '_1.npy',
             os.path.basename(os.path.normpath(path)) + '_1.npz',
             os.path.basename(os.path.normpath(path)) + '_2.avi',
             os.path.basename(os.path.normpath(path)) + '_2.npy',
             os.path.basename(os.path.normpath(path)) + '_2.npz']
    for path_to_delete in path_generator(video_path, files):
        os.remove(path_to_delete)
    try:
        send2trash_original(path)
    except:
        if os.path.isdir(path):
            shutil.rmtree(path)
        else:
            os.remove(path)
