#!/usr/bin/env bash
cp -v --backup=t bpod_base.py $HOME/pybpod/base/pybpod-api/pybpodapi/bpod/bpod_base.py
cp -v --backup=t session.py $HOME/pybpod/base/pybpod-api/pybpodapi/session.py
cp -v --backup=t setup_com.py $HOME/pybpod/base/pybpod-gui-api/pybpodgui_api/models/setup/setup_com.py
cp -v --backup=t board_com.py $HOME/pybpod/base/pybpod-gui-api/pybpodgui_api/models/board/board_com.py
cp -v --backup=t project_io.py $HOME/pybpod/base/pybpod-gui-api/pybpodgui_api/models/project/project_io.py
cp -v --backup=t experiment_io.py $HOME/pybpod/base/pybpod-gui-api/pybpodgui_api/models/experiment/experiment_io.py
cp -v --backup=t setup_io.py $HOME/pybpod/base/pybpod-gui-api/pybpodgui_api/models/setup/setup_io.py
cp -v --backup=t send2trash_wrapper.py $HOME/pybpod/base/pybpod-gui-api/pybpodgui_api/utils/send2trash_wrapper.py
