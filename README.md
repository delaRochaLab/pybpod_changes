# pybpod changes

Package that makes some small modifications to the pybpod gui.

## How to use it

1. Open a terminal window and go to a suitable location.

2. Download the package:

    `git clone https://delaRochaLab@bitbucket.org/delaRochaLab/pybpod_changes.git`
    
3. Go to the package directory.

    `cd pybpod_changes`
    
4. There are 2 directories inside `pybpod_changes`: `version1.6` and `version1.8`. Go to the directory that 
matches your pybpod version. For example:

    `cd version1.6`
    
5. Execute a bash script that will make a backup of some files and then will overwrite them.

    `bash copy_files.sh`


## Files and changes

`base/pybpod-api/pybpodapi/bpod/bpod_base.py`

- when there is an error reading the state machine, send and run the state machine again to avoid a crash

`base/pybpod-api/pybpodapi/session.py`

- create log files in home/error_logs/ with info and exceptions of all the sessions

`base/pybpod-gui-api/pybpodgui_api/models/setup/setup_com.py`

- automatically save the experiment when running
- automatically open the board console when running if you include the following line in the user settings file   
OPEN_CONSOLE_ON_RUN = True
- automatically ask for user when running  
(if user is not selected)

`base/pybpod-gui-api/pybpodgui_api/models/setup/setup_io.py`

- loading a maximum number of sessions per setup if you include the following line in the user settings file        
MAX_NUMBER_OF_SESSIONS = 10

`base/pybpod-gui-api/pybpodgui_api/models/board/board_com.py`

- avoid saving sessions for some tasks included in the user settings file   
NON_SAVING_TASKS = ['daily_check', 'purgevalves', 'water_calibration_task']

`base/pybpod-gui-api/pybpodgui_api/models/project/project_io.py`

- sort the users, boards, tasks, subjects and experiments in alphabetical order
- avoid loading some tasks included in the user settings file   
HIDDEN_TASKS = ['old_task1', 'old_task2']

`base/pybpod-gui-api/pybpodgui_api/models/experiment/experiment_io.py`

- sort the setups in alphabetical order

`base/pybpod-gui-api/pybpodgui_api/utils/send2trash_wrapper.py`

- delete video when deleting a session